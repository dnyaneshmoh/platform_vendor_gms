PRODUCT_COPY_FILES += \
    vendor/gms/system/blobs/etc/permissions/GoogleExtServices_permissions.xml:system/etc/permissions/GoogleExtServices_permissions.xml \
    vendor/gms/system/blobs/etc/permissions/GoogleNetworkStack_permissions.xml:system/etc/permissions/GoogleNetworkStack_permissions.xml \
    vendor/gms/system/blobs/etc/permissions/GooglePermissionController_permissions.xml:system/etc/permissions/GooglePermissionController_permissions.xml \
    vendor/gms/system/blobs/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml
