# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    CarrierMetrics \
    Chrome \
    DevicePolicyPrebuilt \
    DiagnosticsToolPrebuilt \
    Drive \
    GoogleCamera \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    Maps \
    MarkupGoogle \
    MicropaperPrebuilt \
    NexusWallpapersStubPrebuilt2019 \
    NgaResources \
    Ornament \
    Photos \
    PixelThemesStub2019 \
    PlayAutoInstallConfig \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundAmplifierPrebuilt \
    SoundPickerPrebuilt \
    TrichromeLibrary \
    Tycho \
    VZWAPNLib \
    WallpapersBReel2019 \
    WebViewGoogle \
    arcore \
    talkback

# product/priv-app
PRODUCT_PACKAGES += \
    AmbientSensePrebuilt \
    AndroidAutoStubPrebuilt \
    AndroidMigratePrebuilt \
    AppDirectedSMSService \
    CarrierLocation \
    CarrierServices \
    CarrierSettings \
    CarrierSetup \
    CarrierWifi \
    ConfigUpdater \
    ConnMO \
    ConnMetrics \
    DMService \
    GCS \
    GoogleDialer \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    MaestroPrebuilt \
    MatchmakerPrebuiltPixel4 \
    MyVerizonServices \
    NexusLauncherRelease \
    OBDM_Permissions \
    OemDmTrigger \
    Phonesky \
    PixelLiveWallpaperPrebuilt \
    PixelSetupWizard \
    PrebuiltGmsCore \
    RecorderPrebuilt \
    RilConfigService \
    SCONE \
    SafetyHubPrebuilt \
    ScribePrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizardPrebuilt \
    Showcase \
    SprintDM \
    SprintHM \
    StorageManagerGoogle \
    TurboPrebuilt \
    USCCDM \
    Velvet \
    VzwOmaTrigger \
    WallpaperPickerGoogleRelease \
    WellbeingPrebuilt \
    WfcActivation \
    grilservice \
    obdm_stub

# system/app
PRODUCT_PACKAGES += \
    GoogleCaptivePortalLogin \
    GoogleExtShared \
    GooglePrintRecommendationService \

# system/priv-app
PRODUCT_PACKAGES += \
    GoogleDocumentsUIPrebuilt \
    GoogleExtServicesPrebuilt \
    GooglePackageInstaller \
    GooglePermissionControllerPrebuilt \
    ModuleMetadataGooglePrebuilt \
    TagGoogle

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreQt \
    PrebuiltGmsCoreQt_GoogleCertificates \
    PrebuiltGmsCoreQt_DynamiteLoader \
    PrebuiltGmsCoreQt_AdsDynamite \
    PrebuiltGmsCoreQt_DynamiteModulesA \
    PrebuiltGmsCoreQt_DynamiteModulesC \
    PrebuiltGmsCoreQt_MeasurementDynamite \
    PrebuiltGmsCoreQt_MapsDynamite \
    PrebuiltGmsCoreQt_CronetDynamite \
    AndroidPlatformServices

PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

$(call inherit-product, vendor/gms/product/blobs/product_blobs.mk)
$(call inherit-product, vendor/gms/system/blobs/system_blobs.mk)

